package com.talat.utility.ui;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;

import com.google.gson.Gson;
import com.talat.utility.R;
import com.talat.utility.model.Example;
import com.talat.utility.rest.ApiInterface;
import com.talat.utility.util.Constant;
import com.talat.utility.util.PreferenceHelper;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;


public class LoginActivity extends AppCompatActivity {
    @BindView(R.id.et_user_name)
    EditText etUserName;
    @BindView(R.id.et_password)
    EditText etPassword;
    Button login;
    private static final String TAG="LoginActivity";
    private ProgressDialog progressDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);
        ButterKnife.bind(this);
        progressDialog = new ProgressDialog(LoginActivity.this,
                R.style.AppTheme_Dark_Dialog);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage(getString(R.string.progress_dialog));

    }
    @OnClick(R.id.btn_login)
    public void login(){
        showProgressBar();
        String username=etUserName.getText().toString();
        String password=etPassword.getText().toString();
        getUser(username,password);
    }
    private void getUser(final String username, String password){
        RestAdapter adapter = new RestAdapter.Builder()
                .setEndpoint(Constant.ROOT_URL)
                .build();
        ApiInterface api = adapter.create(ApiInterface.class);
        final LoginRequest loginRequest=new LoginRequest(username,password);
        api.getLogin(loginRequest, new Callback<Example>() {
                    @Override
                    public void success(Example example, Response response) {
                        hideProgressBar();
                        Gson gson = new Gson();
                        PreferenceHelper preferenceHelper = new PreferenceHelper(LoginActivity.this);
                        String jsonUser = gson.toJson(example);
                        String jsonLogin=gson.toJson(loginRequest);
                        preferenceHelper.setPrefrance(Constant.KEY_PRF_STUART, jsonUser);
                        preferenceHelper.setPrefrance(Constant.LOGIN_DETAILS,jsonLogin);
                        Intent intent=new Intent(LoginActivity.this,HomeActivity.class);
                        intent.putExtra("List",example);
                        startActivity(intent);


                    }

                    @Override
                    public void failure(RetrofitError error) {
                        hideProgressBar();
                        Log.d(TAG,error.toString());
                    }
                });
    }
    private void showProgressBar(){
        progressDialog.show();
    }
    private void hideProgressBar(){
        progressDialog.dismiss();
    }
}
