package com.talat.utility.ui;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.talat.utility.R;
import com.talat.utility.model.StopPayment;
import com.talat.utility.model.TodayPaymentList;
import com.talat.utility.model.TomorrowPaymentList;
import com.talat.utility.rest.ApiInterface;
import com.talat.utility.util.Constant;
import com.talat.utility.util.PreferenceHelper;

import java.text.Format;
import java.text.NumberFormat;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class DetailActivity extends AppCompatActivity {
    private static final String TAG = "DetailActivity";
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.tv_account_no)
    TextView tvAccountNo;
    @BindView(R.id.tv_address)
    TextView tvAddress;
    @BindView(R.id.tv_amount)
    TextView tvAmount;
    @BindView(R.id.tv_bill_cycle)
    TextView tvBillCycle;
    @BindView(R.id.tv_before_day)
    TextView tvBeforeDay;
    @BindView(R.id.tv_bill_generating_date)
    TextView tvBillGeneratingDate;
    @BindView(R.id.tv_email)
    TextView tvEmail;
    @BindView(R.id.tv_location)
    TextView tvLocation;
    @BindView(R.id.tv_mobile_no)
    TextView tvMobileNumber;
    @BindView(R.id.tv_name_of_owner)
    TextView tvNameOfOwner;
    @BindView(R.id.btn_stop_payment)
    Button btnStopPayment;
    TodayPaymentList todayPaymentList;
    TomorrowPaymentList tomorrowPaymentList;
    ProgressDialog progressDialog;
    LoginRequest loginRequest;
    private String utilityBillId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        ButterKnife.bind(this);
        setToolbar();
        progressDialog = new ProgressDialog(DetailActivity.this,
                R.style.AppTheme_Dark_Dialog);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage(getString(R.string.progress_dialog));
        PreferenceHelper preferenceHelper = new PreferenceHelper(DetailActivity.this);
        String guestJson = preferenceHelper.getPrefrance(Constant.LOGIN_DETAILS);
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
        loginRequest = gson.fromJson(guestJson, LoginRequest.class);
        Log.d(TAG, loginRequest.toString());
        boolean today = getIntent().getExtras().getBoolean("today");
        if (today == true) {
            todayPaymentList = getIntent().getParcelableExtra("PaymentList");
            Log.d(TAG, todayPaymentList.toString());
            String status = todayPaymentList.getStatus();
            checkStatus(status);
            utilityBillId = todayPaymentList.getBillId();
            setList(true);
        } else {
            tomorrowPaymentList = getIntent().getParcelableExtra("PaymentList");
            String status = tomorrowPaymentList.getStatus();
            checkStatus(status);
            utilityBillId = tomorrowPaymentList.getBillId();
            Log.d(TAG, tomorrowPaymentList.toString());
            setList(false);
        }

    }

    public void setList(boolean value) {
        if (value == true) {
            tvAccountNo.setText(todayPaymentList.getAccountNo());
            tvAddress.setText(todayPaymentList.getAddress());
            String amount=todayPaymentList.getAmounts();
            if(amount.isEmpty()){

            }
            else{
                Double price= Double.parseDouble(todayPaymentList.getAmounts());
                Format format = NumberFormat.getCurrencyInstance(new Locale("en", "in"));
                tvAmount.setText(format.format(price));
            }

            tvBillCycle.setText(todayPaymentList.getBIllCycle());
            tvBeforeDay.setText(todayPaymentList.getBeforeDay());
            tvBillGeneratingDate.setText(todayPaymentList.getBillGeneratingDate());
            tvEmail.setText(todayPaymentList.getEmail());
            tvLocation.setText(todayPaymentList.getLocation());
            tvMobileNumber.setText(todayPaymentList.getMobileNo());
            tvNameOfOwner.setText(todayPaymentList.getNameOfOwner());
        } else {
            tvAccountNo.setText(tomorrowPaymentList.getAccountNo());
            tvAddress.setText(tomorrowPaymentList.getAddress());
            String amount=tomorrowPaymentList.getAmounts();
            if(amount.isEmpty()){

            }
            else{
                Double price= Double.parseDouble(tomorrowPaymentList.getAmounts());
                Format format = NumberFormat.getCurrencyInstance(new Locale("en", "in"));
                tvAmount.setText(format.format(price));
            }
            tvBillCycle.setText(tomorrowPaymentList.getBIllCycle());
            tvBeforeDay.setText(tomorrowPaymentList.getBeforeDay());
            tvBillGeneratingDate.setText(tomorrowPaymentList.getBillGeneratingDate());
            tvEmail.setText(tomorrowPaymentList.getEmail());
            tvLocation.setText(tomorrowPaymentList.getLocation());
            tvMobileNumber.setText(tomorrowPaymentList.getMobileNo());
            tvNameOfOwner.setText(tomorrowPaymentList.getNameOfOwner());
        }
    }

    public void checkStatus(String value) {
        Log.d(TAG, value);
        if (value.equals("0"))
            btnStopPayment.setVisibility(View.GONE);

    }

    private void setToolbar() {
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        }
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    @OnClick(R.id.btn_stop_payment)
    public void stopPayment() {
        showProgressBar();
        RestAdapter adapter = new RestAdapter.Builder()
                .setEndpoint(Constant.ROOT_URL)
                .build();
        ApiInterface api = adapter.create(ApiInterface.class);
        StopStatus stopStatus = new StopStatus(loginRequest.getUser_name(), loginRequest.getPassword(), utilityBillId);
        api.stopStatus(stopStatus, new Callback<StopPayment>() {
            @Override
            public void success(StopPayment stopPayment, Response response) {
                hideProgressBar();
                Log.d(TAG, stopPayment.toString());
                boolean status = stopPayment.getAccountstatus();
                if (status = true) {
                    Toast.makeText(DetailActivity.this, "Success", Toast.LENGTH_SHORT).show();
                    Intent intent= new Intent(DetailActivity.this,SplashActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                    finish();
                } else {
                    Toast.makeText(DetailActivity.this, "Not Updated", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void failure(RetrofitError error) {
                Toast.makeText(DetailActivity.this, error.toString(), Toast.LENGTH_SHORT).show();
                hideProgressBar();
            }
        });

    }

    private void showProgressBar() {
        progressDialog.show();
    }

    private void hideProgressBar() {
        progressDialog.dismiss();
    }

}
