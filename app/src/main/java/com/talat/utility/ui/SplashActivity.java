package com.talat.utility.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.talat.utility.R;
import com.talat.utility.model.Example;
import com.talat.utility.rest.ApiInterface;
import com.talat.utility.util.Constant;
import com.talat.utility.util.PreferenceHelper;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class SplashActivity extends AppCompatActivity {
    private static final String TAG="SplashActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        PreferenceHelper preferenceHelper = new PreferenceHelper(SplashActivity.this);
        String guestJson = preferenceHelper.getPrefrance(Constant.LOGIN_DETAILS);
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
        if (guestJson != null) {
           final LoginRequest loginRequest = gson.fromJson(guestJson, LoginRequest.class);
            RestAdapter adapter = new RestAdapter.Builder()
                    .setEndpoint(Constant.ROOT_URL)
                    .build();
            ApiInterface api = adapter.create(ApiInterface.class);
            api.getLogin(loginRequest, new Callback<Example>() {
                @Override
                public void success(Example example, Response response) {
                    Gson gson = new Gson();
                    PreferenceHelper preferenceHelper = new PreferenceHelper(SplashActivity.this);
                    String jsonUser = gson.toJson(example);
                    String jsonLogin=gson.toJson(loginRequest);
                    preferenceHelper.setPrefrance(Constant.KEY_PRF_STUART, jsonUser);
                    preferenceHelper.setPrefrance(Constant.LOGIN_DETAILS,jsonLogin);
                    Intent intent=new Intent(SplashActivity.this,HomeActivity.class);
                    intent.putExtra("List",example);
                    startActivity(intent);
                    finish();
                }

                @Override
                public void failure(RetrofitError error) {
                    Log.d(TAG,error.toString());
                }
            });


            } else {
                Intent mIntent = new Intent(SplashActivity.this, LoginActivity.class);
                startActivity(mIntent);
                finish();
            }

        }

}
