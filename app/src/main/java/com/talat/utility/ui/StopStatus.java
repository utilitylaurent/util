package com.talat.utility.ui;

/**
 * Created by talat on 12-01-2017.
 */
public class StopStatus {
    private String user_name;
    private String password;
    private String utility_bill_id;

    public StopStatus(String user_name, String password, String utility_bill_id) {
        this.user_name = user_name;
        this.password = password;
        this.utility_bill_id = utility_bill_id;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUtility_bill_id() {
        return utility_bill_id;
    }

    public void setUtility_bill_id(String utility_bill_id) {
        this.utility_bill_id = utility_bill_id;
    }

    @Override
    public String toString() {
        return "StopStatus{" +
                "user_name='" + user_name + '\'' +
                ", password='" + password + '\'' +
                ", utility_bill_id='" + utility_bill_id + '\'' +
                '}';
    }
}
