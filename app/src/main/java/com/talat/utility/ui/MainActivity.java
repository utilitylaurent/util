package com.talat.utility.ui;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.talat.utility.R;
import com.talat.utility.model.TodayPaymentList;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity {
    @BindView(R.id.list)
    RecyclerView recycleList;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        List<TodayPaymentList> list = (List<TodayPaymentList>) getIntent().getSerializableExtra("TodayList");
        Log.d("test",list.toString());
    }
}
