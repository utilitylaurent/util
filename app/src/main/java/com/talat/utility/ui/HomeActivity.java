package com.talat.utility.ui;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.talat.utility.R;
import com.talat.utility.adapter.PagerAdapter;
import com.talat.utility.fragment.TodayFragment;
import com.talat.utility.fragment.TomorrowFragment;
import com.talat.utility.model.Example;
import com.talat.utility.rest.ApiInterface;
import com.talat.utility.util.Constant;
import com.talat.utility.util.PreferenceHelper;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class HomeActivity extends AppCompatActivity {
    ViewPager mViewPager;
    TabLayout tabLayout;
    Toolbar toolbar;
    private static final String TAG="HomeActivity";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        toolbar=(Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        mViewPager=(ViewPager)findViewById(R.id.view_pager);
        tabLayout=(TabLayout)findViewById(R.id.tab_layout);
        Example example=getIntent().getParcelableExtra("List");
        setPagerAdapter(example);

    }
    public void setPagerAdapter(Example example) {
        FragmentManager manager=getSupportFragmentManager();
        PagerAdapter adapter=new PagerAdapter(manager);
        TodayFragment todayFragment=new TodayFragment();
        Bundle todayBundle=new Bundle();
        todayBundle.putParcelable(getString(R.string.today_list),example);
        todayFragment.setArguments(todayBundle);
        Bundle tomorrowBundle=new Bundle();
        tomorrowBundle.putParcelable(getString(R.string.tomorrow_list),example);
        TomorrowFragment tomorrowFragment =new TomorrowFragment();
        tomorrowFragment.setArguments(tomorrowBundle);
        adapter.addFragment(todayFragment, getString(R.string.today));
        adapter.addFragment(tomorrowFragment, getString(R.string.tomorrow));
        mViewPager.setAdapter(adapter);
        tabLayout.setupWithViewPager(mViewPager);
        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {

                switch (position) {
                    case 0:

                        break;

                    case 1:

                        break;

                    case 2:

                        break;
                    case 3:

                        break;
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    @Override
    public void onResume(){
        super.onResume();
        RestAdapter adapter = new RestAdapter.Builder()
                .setEndpoint(Constant.ROOT_URL)
                .build();
        ApiInterface api = adapter.create(ApiInterface.class);
        PreferenceHelper preferenceHelper = new PreferenceHelper(HomeActivity.this);
        String guestJson = preferenceHelper.getPrefrance(Constant.LOGIN_DETAILS);
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
        final LoginRequest loginRequest=gson.fromJson(guestJson, LoginRequest.class);
        api.getLogin(loginRequest, new Callback<Example>() {
            @Override
            public void success(Example example, Response response) {
                setPagerAdapter(example);
            }

            @Override
            public void failure(RetrofitError error) {
                Log.d(TAG,error.toString());
            }
        });

    }



 }
