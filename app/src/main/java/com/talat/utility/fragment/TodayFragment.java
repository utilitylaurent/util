package com.talat.utility.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.talat.utility.R;
import com.talat.utility.adapter.TodayAdapter;
import com.talat.utility.model.Example;
import com.talat.utility.model.TodayPaymentList;
import com.talat.utility.ui.DetailActivity;
import com.talat.utility.util.RecyclerItemClickListener;

import java.util.List;


public class TodayFragment extends Fragment {
    Context context;
    List<TodayPaymentList> list;
//bfdgdgdfgd
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_demo, container, false);
        Bundle bundle = getArguments();
        Example example = bundle.getParcelable(getString(R.string.today_list));
        list = example.getTodayPaymentList();
        RecyclerView recyclerView = (RecyclerView) rootView.findViewById(R.id.list);
        recyclerView.setHasFixedSize(true);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(context, 1);
        recyclerView.setLayoutManager(gridLayoutManager);
        TodayAdapter todayAdapter = new TodayAdapter(list);
        todayAdapter.notifyDataSetChanged();
        recyclerView.setAdapter(todayAdapter);
        recyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(context, new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        Intent intent = new Intent(getActivity(), DetailActivity.class);
                        TodayPaymentList todayPaymentList = list.get(position);
                        intent.putExtra("PaymentList", todayPaymentList);
                        intent.putExtra("today",true);
                        startActivity(intent);
                    }
                })
        );
        recyclerView.refreshDrawableState();
        return rootView;
}

}