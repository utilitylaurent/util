package com.talat.utility.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.talat.utility.R;
import com.talat.utility.adapter.TomorrowAdapter;
import com.talat.utility.model.Example;
import com.talat.utility.model.TomorrowPaymentList;
import com.talat.utility.ui.DetailActivity;
import com.talat.utility.util.RecyclerItemClickListener;

import java.util.List;

public class TomorrowFragment extends Fragment {
    Context context;
    List<TomorrowPaymentList> list;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.tomorrowfragment, container, false);
        Bundle bundle = getArguments();
        Example example = bundle.getParcelable(getString(R.string.tomorrow_list));
        list = example.getTomorrowPaymentList();
        Log.d("test", list.toString());
        RecyclerView recyclerView = (RecyclerView) rootView.findViewById(R.id.list);
        recyclerView.setHasFixedSize(true);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(context, 1);
        recyclerView.setLayoutManager(gridLayoutManager);
        TomorrowAdapter todayAdapter = new TomorrowAdapter(list);
        todayAdapter.notifyDataSetChanged();
        recyclerView.setAdapter(todayAdapter);
        recyclerView.addOnItemTouchListener(new RecyclerItemClickListener(context, new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        Intent intent = new Intent(getActivity(), DetailActivity.class);
                        TomorrowPaymentList tomorrowPaymentList = list.get(position);
                        intent.putExtra("PaymentList", tomorrowPaymentList);
                        intent.putExtra("today",false);
                        startActivity(intent);
                    }
                })
        );
        return rootView;
}

}
