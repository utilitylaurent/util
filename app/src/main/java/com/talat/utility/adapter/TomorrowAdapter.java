package com.talat.utility.adapter;

import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.talat.utility.R;
import com.talat.utility.model.TomorrowPaymentList;

import java.text.Format;
import java.text.NumberFormat;
import java.util.List;
import java.util.Locale;

/**
 * Created by talat on 10-01-2017.
 */
public class TomorrowAdapter extends RecyclerView.Adapter<TomorrowAdapter.ContactViewHolder> {

    private List<TomorrowPaymentList> tomorrowPaymentLists;
    public final String TAG = "TomorrowAdapter";

    public TomorrowAdapter(List<TomorrowPaymentList> list) {
        this.tomorrowPaymentLists = list;
    }

    public static class ContactViewHolder extends RecyclerView.ViewHolder {
        protected TextView tvAccount;
        protected TextView tvLocation;
        protected TextView tvPrice;
        protected TextView tvStatus;

        public ContactViewHolder(View v) {
            super(v);
            tvAccount = (TextView) v.findViewById(R.id.tv_account_no);
            tvLocation = (TextView) v.findViewById(R.id.tv_location);
            tvPrice = (TextView) v.findViewById(R.id.tv_price);
            tvStatus=(TextView)v.findViewById(R.id.tv_status);
        }
    }

    @Override
    public ContactViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View itemView = LayoutInflater.
                from(viewGroup.getContext()).
                inflate(R.layout.today_list, viewGroup, false);
        return new ContactViewHolder(itemView);

    }

    @Override
    public void onBindViewHolder(ContactViewHolder holder, int position) {
        TomorrowPaymentList list = tomorrowPaymentLists.get(position);
        holder.tvAccount.setText(list.getAccountNo());
        holder.tvLocation.setText(list.getLocation());
        String amount=list.getAmounts();
        if(amount.isEmpty()){

        }
        else{
            Double price= Double.parseDouble(list.getAmounts());
            Format format = NumberFormat.getCurrencyInstance(new Locale("en", "in"));
            holder.tvPrice.setText(format.format(price));
        }

        String status=list.getStatus();
        if(status.equals("0")){
            holder.tvStatus.setText("Service Stoped");
            holder.tvStatus.setTextColor(Color.RED);
        }
        else{
            holder.tvStatus.setText("Active");
            holder.tvStatus.setTextColor(Color.GREEN);
        }


    }

    @Override
    public int getItemCount() {
        return tomorrowPaymentLists.size();
    }


}
