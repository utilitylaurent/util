
package com.talat.utility.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Example implements Parcelable {

    @SerializedName("TodayPayment_List")
    @Expose
    private List<TodayPaymentList> todayPaymentList = null;
    @SerializedName("TomorrowPayment_List")
    @Expose
    private List<TomorrowPaymentList> tomorrowPaymentList = null;
    @SerializedName("success")
    @Expose
    private Boolean success;

    protected Example(Parcel in) {
        todayPaymentList = in.createTypedArrayList(TodayPaymentList.CREATOR);
        tomorrowPaymentList = in.createTypedArrayList(TomorrowPaymentList.CREATOR);
    }

    public static final Creator<Example> CREATOR = new Creator<Example>() {
        @Override
        public Example createFromParcel(Parcel in) {
            return new Example(in);
        }

        @Override
        public Example[] newArray(int size) {
            return new Example[size];
        }
    };

    public List<TodayPaymentList> getTodayPaymentList() {
        return todayPaymentList;
    }

    public void setTodayPaymentList(List<TodayPaymentList> todayPaymentList) {
        this.todayPaymentList = todayPaymentList;
    }

    public List<TomorrowPaymentList> getTomorrowPaymentList() {
        return tomorrowPaymentList;
    }

    public void setTomorrowPaymentList(List<TomorrowPaymentList> tomorrowPaymentList) {
        this.tomorrowPaymentList = tomorrowPaymentList;
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeTypedList(todayPaymentList);
        parcel.writeTypedList(tomorrowPaymentList);
    }
}
