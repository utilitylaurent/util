package com.talat.utility.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by talat on 12-01-2017.
 */
public class StopPayment {
    @SerializedName("Accountstatus")
    @Expose
    private Boolean accountstatus;

    public Boolean getAccountstatus() {
        return accountstatus;
    }

    public void setAccountstatus(Boolean accountstatus) {
        this.accountstatus = accountstatus;
    }

    @Override
    public String toString() {
        return "StopPayment{" +
                "accountstatus=" + accountstatus +
                '}';
    }
}
