
package com.talat.utility.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TodayPaymentList implements Parcelable{

    @SerializedName("Account_No")
    @Expose
    private String accountNo;

    @Override
    public String toString() {
        return "TodayPaymentList{" +
                "accountNo='" + accountNo + '\'' +
                ", address='" + address + '\'' +
                ", amounts='" + amounts + '\'' +
                ", bIllCycle='" + bIllCycle + '\'' +
                ", beforeDay='" + beforeDay + '\'' +
                ", billGeneratingDate='" + billGeneratingDate + '\'' +
                ", email='" + email + '\'' +
                ", location='" + location + '\'' +
                ", mobileNo='" + mobileNo + '\'' +
                ", nameOfOwner='" + nameOfOwner + '\'' +
                ", password='" + password + '\'' +
                ", plans='" + plans + '\'' +
                ", remarks='" + remarks + '\'' +
                ", roomNo='" + roomNo + '\'' +
                ", roomInside='" + roomInside + '\'' +
                ", serviceNo='" + serviceNo + '\'' +
                ", billId='" + billId + '\'' +
                ", paymentDueDate='" + paymentDueDate + '\'' +
                ", paymentmode='" + paymentmode + '\'' +
                ", property='" + property + '\'' +
                ", providername='" + providername + '\'' +
                ", servicetype='" + servicetype + '\'' +
                ", status='" + status + '\'' +
                ", userid='" + userid + '\'' +
                '}';
    }

    @SerializedName("Address")
    @Expose
    private String address;
    @SerializedName("Amounts")
    @Expose
    private String amounts;
    @SerializedName("BIll_Cycle")
    @Expose
    private String bIllCycle;
    @SerializedName("BeforeDay")
    @Expose
    private String beforeDay;
    @SerializedName("Bill_generating_date")
    @Expose
    private String billGeneratingDate;
    @SerializedName("Email")
    @Expose
    private String email;
    @SerializedName("Location")
    @Expose
    private String location;
    @SerializedName("Mobile_no")
    @Expose
    private String mobileNo;
    @SerializedName("NameOfOwner")
    @Expose
    private String nameOfOwner;
    @SerializedName("Password")
    @Expose
    private String password;
    @SerializedName("Plans")
    @Expose
    private String plans;
    @SerializedName("Remarks")
    @Expose
    private String remarks;
    @SerializedName("Room_No")
    @Expose
    private String roomNo;
    @SerializedName("Room_inside")
    @Expose
    private String roomInside;
    @SerializedName("Service_No")
    @Expose
    private String serviceNo;
    @SerializedName("bill_id")
    @Expose
    private String billId;
    @SerializedName("payment_due_date")
    @Expose
    private String paymentDueDate;
    @SerializedName("paymentmode")
    @Expose
    private String paymentmode;
    @SerializedName("property")
    @Expose
    private String property;
    @SerializedName("providername")
    @Expose
    private String providername;
    @SerializedName("servicetype")
    @Expose
    private String servicetype;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("userid")
    @Expose
    private String userid;

    protected TodayPaymentList(Parcel in) {
        accountNo = in.readString();
        address = in.readString();
        amounts = in.readString();
        bIllCycle = in.readString();
        beforeDay = in.readString();
        billGeneratingDate = in.readString();
        email = in.readString();
        location = in.readString();
        mobileNo = in.readString();
        nameOfOwner = in.readString();
        password = in.readString();
        plans = in.readString();
        remarks = in.readString();
        roomNo = in.readString();
        roomInside = in.readString();
        serviceNo = in.readString();
        billId = in.readString();
        paymentDueDate = in.readString();
        paymentmode = in.readString();
        property = in.readString();
        providername = in.readString();
        servicetype = in.readString();
        status = in.readString();
        userid = in.readString();
    }

    public static final Creator<TodayPaymentList> CREATOR = new Creator<TodayPaymentList>() {
        @Override
        public TodayPaymentList createFromParcel(Parcel in) {
            return new TodayPaymentList(in);
        }

        @Override
        public TodayPaymentList[] newArray(int size) {
            return new TodayPaymentList[size];
        }
    };

    public String getAccountNo() {
        return accountNo;
    }

    public void setAccountNo(String accountNo) {
        this.accountNo = accountNo;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getAmounts() {
        return amounts;
    }

    public void setAmounts(String amounts) {
        this.amounts = amounts;
    }

    public String getBIllCycle() {
        return bIllCycle;
    }

    public void setBIllCycle(String bIllCycle) {
        this.bIllCycle = bIllCycle;
    }

    public String getBeforeDay() {
        return beforeDay;
    }

    public void setBeforeDay(String beforeDay) {
        this.beforeDay = beforeDay;
    }

    public String getBillGeneratingDate() {
        return billGeneratingDate;
    }

    public void setBillGeneratingDate(String billGeneratingDate) {
        this.billGeneratingDate = billGeneratingDate;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public String getNameOfOwner() {
        return nameOfOwner;
    }

    public void setNameOfOwner(String nameOfOwner) {
        this.nameOfOwner = nameOfOwner;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPlans() {
        return plans;
    }

    public void setPlans(String plans) {
        this.plans = plans;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getRoomNo() {
        return roomNo;
    }

    public void setRoomNo(String roomNo) {
        this.roomNo = roomNo;
    }

    public String getRoomInside() {
        return roomInside;
    }

    public void setRoomInside(String roomInside) {
        this.roomInside = roomInside;
    }

    public String getServiceNo() {
        return serviceNo;
    }

    public void setServiceNo(String serviceNo) {
        this.serviceNo = serviceNo;
    }

    public String getBillId() {
        return billId;
    }

    public void setBillId(String billId) {
        this.billId = billId;
    }

    public String getPaymentDueDate() {
        return paymentDueDate;
    }

    public void setPaymentDueDate(String paymentDueDate) {
        this.paymentDueDate = paymentDueDate;
    }

    public String getPaymentmode() {
        return paymentmode;
    }

    public void setPaymentmode(String paymentmode) {
        this.paymentmode = paymentmode;
    }

    public String getProperty() {
        return property;
    }

    public void setProperty(String property) {
        this.property = property;
    }

    public String getProvidername() {
        return providername;
    }

    public void setProvidername(String providername) {
        this.providername = providername;
    }

    public String getServicetype() {
        return servicetype;
    }

    public void setServicetype(String servicetype) {
        this.servicetype = servicetype;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(accountNo);
        parcel.writeString(address);
        parcel.writeString(amounts);
        parcel.writeString(bIllCycle);
        parcel.writeString(beforeDay);
        parcel.writeString(billGeneratingDate);
        parcel.writeString(email);
        parcel.writeString(location);
        parcel.writeString(mobileNo);
        parcel.writeString(nameOfOwner);
        parcel.writeString(password);
        parcel.writeString(plans);
        parcel.writeString(remarks);
        parcel.writeString(roomNo);
        parcel.writeString(roomInside);
        parcel.writeString(serviceNo);
        parcel.writeString(billId);
        parcel.writeString(paymentDueDate);
        parcel.writeString(paymentmode);
        parcel.writeString(property);
        parcel.writeString(providername);
        parcel.writeString(servicetype);
        parcel.writeString(status);
        parcel.writeString(userid);
    }
}
