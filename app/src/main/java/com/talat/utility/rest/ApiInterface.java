package com.talat.utility.rest;

import com.talat.utility.model.Example;
import com.talat.utility.model.StopPayment;
import com.talat.utility.ui.LoginRequest;
import com.talat.utility.ui.StopStatus;

import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.Headers;
import retrofit.http.POST;


/**
 * Created by talat on 09-01-2017.
 */
public interface ApiInterface {

    @Headers("Content-Type: application/json")
    @POST("/UserLogin")
     public void getLogin(@Body LoginRequest task, Callback<Example> response);

    @Headers("Content-Type: application/json")
    @POST("/StopStatus")
    public void stopStatus(@Body StopStatus task, Callback<StopPayment> response);

}
